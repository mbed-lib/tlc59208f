/*
 * PCA9624.cpp
 * Copyright (C) 2022 Adrian Badoil 
 *
 * Distributed under terms of the MIT license.
 */

#include "mbed.h"
#include "TLC59208.h"
#include <cstdint>


TLC59208::TLC59208(I2C *i2c, char deviceAddress, const unsigned int p_frequency) {
    _i2c = i2c;
    _writeOpcode = deviceAddress & 0xFE; // low order bit = 0 for write
    _readOpcode  = deviceAddress | 0x01; // low order bit = 1 for read
    _i2c->frequency(p_frequency); // Set the frequency of the I5C interface
    init();
}

char TLC59208::_read(char address) {
    char data[2];

    data[0] = address;
    _i2c->write(_writeOpcode, data, 1);     // Select Register for reading
    _i2c->read(_readOpcode, data, 1);       // Read from selected Register

    return data[0];
}

bool TLC59208::_write(char address, char byte) {
    char data[2];

    data[0] = address;
    data[1] = byte;
    return _i2c->write(_writeOpcode, data, 2) == 0;  // Write data to selected register
}

bool TLC59208::init() {
    bool ret = 0; //return 1 in case of success
    ret = sleep(false);
    if (ret != 1) {
        return ret;
    }
    char state = PWM_EACH_AND_GROUP;
    char data = ((uint8_t)state << 6) | ((uint8_t)state << 4) | ((uint8_t)state << 2) | ((uint8_t)state << 0);
    ret = _write(LEDOUT0, data);
    if (ret != 1) {
        return ret;
    }
    return _write(LEDOUT1, data);
}

bool TLC59208::drive(const char ch, const char vol) {
    char channel = PWM0 + ch;
    return _write(channel, vol);
}

bool TLC59208::sleep(const bool b) {
    char data = 0;
    if (b)
        data = 0b10010001;
    else
        data = 0b10000001;
    return _write(MODE1, data);
}

bool TLC59208::setGroupPWM(const char vol) {
    return _write(GRPPWM, vol);
}

bool TLC59208::reset(){
    char data[2];
    char adrs = TLC59208F_SWRST_ADDR;
    data[0] = TLC59208F_SWRST_SEQ[0];
    data[1] = TLC59208F_SWRST_SEQ[1];
    return _i2c->write(adrs, data, 2);     // Select Register for reading
}
