/*
 * TLC59208F I2C LED control IC by Texas Instrument.
 *
 * Adrian Badoil <adrian<badoil@protonmail.com>
 */

#ifndef TLC59208_H
#define TLC59208_H

#include <mbed.h>
#include "SPI.h"
#include <util/Bitfield.h>

// All register addresses assume IOCON.BANK = 0 (POR default)
#define MODE1      0x00
#define MODE2      0x01
#define PWM0       0x02
#define PWM1       0x03
#define PWM2       0x04
#define PWM3       0x05
#define PWM4       0x06
#define PWM5       0x07
#define PWM6       0x08
#define PWM7       0x09
#define GRPPWM     0x0a
#define GRPFREQ    0x0b
#define LEDOUT0    0x0c
#define LEDOUT1    0x0d
#define SUBADR1    0x0e
#define SUBADR2    0x0f
#define SUBADR3    0x10
#define ALLCALLADR 0x11

// Output Mode
#define DISABLE            0x00
#define FULLY_ON           0x01
#define PWM_EACH           0x02
#define PWM_EACH_AND_GROUP 0x03

// Call address to perform software reset, no devices will ACK
const uint8_t TLC59208F_SWRST_ADDR = 0x96;  //(0x4b 7-bit addr + ~W)
const uint8_t TLC59208F_SWRST_SEQ[2] = {0xa5, 0x5a};

class TLC59208 {
public:
    /** Create an TLC59208 object connected to the specified I2C object and using the specified deviceAddress
    *
    * @param I2C &i2c the I2C port to connect to
    * @param char deviceAddress the address of the MCP23017
    */
    TLC59208(I2C *i2c, char deviceAddress=0x40, const unsigned int p_frequency = 100000);
    
    //bool setDriverOutputState(uint8_t state);
        /** Init TLC59208
    *
    * @param
    * @returns true on success, false otherwise
    */
    bool init();
    bool drive(const char ch, const char vol);
    bool sleep(const bool b);
    bool setGroupPWM(const char vol);
    bool reset();

protected:
    I2C *_i2c;
    char _readOpcode;
    char _writeOpcode;

    /** Init TLC59208
    *
    * @param
    * @returns true on success, false otherwise
    */
    bool _init();

    /** Write to specified TLC59208 register
    *
    * @param char address the internal registeraddress of the PCA9624
    * @returns true on success, false otherwise
    */
    bool _write(char address, char byte);

    /** Read from specified TLC59208 register
    *
    * @param char address the internal registeraddress of the PCA9624
    * @returns data from register
    */
    char _read(char address);
};

#endif //TMC4210_H
